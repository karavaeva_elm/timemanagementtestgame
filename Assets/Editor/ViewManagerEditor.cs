﻿using UnityEditor;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using UnityEngine.UI;

[CustomEditor(typeof(ViewManager))]
public class ViewManagerEditor : Editor
{
    ViewManager viewManager;
    void OnEnable()
    {
        viewManager = (ViewManager)target;
    }
    public override void OnInspectorGUI()
    {
        viewManager.mySpriteType = (SpriteType)EditorGUILayout.EnumPopup("Type", viewManager.mySpriteType);
        switch (viewManager.mySpriteType)
        {
            case SpriteType.Renderer:
                {
                    viewManager.myRenderer = (SpriteRenderer)EditorGUILayout.ObjectField("Sprite Renderer", viewManager.myRenderer, typeof(SpriteRenderer), true);
                    break;
                }
            case SpriteType.Image:
                {
                    viewManager.myImg = (Image)EditorGUILayout.ObjectField("Image", viewManager.myImg, typeof(Image), true);
                    break;
                }
        }
    }
}