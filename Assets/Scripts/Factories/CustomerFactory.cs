﻿using System.Collections;
using System.Collections.Generic;
using Cooking;
using UnityEngine;

namespace Factory
{
    public class CustomerFactory : Factory<Customer>
    {
        [SerializeField]
        float customerWaitTime;
        [SerializeField]
        string[] spriteKeys;
        private Customer BindCustomer(System.Object[] parameters)
        {
            return new Customer((string)parameters[0], (float)parameters[1], (Cooking.DishInfo[])parameters[2]);
        }

        protected override Customer Create(params System.Object[] parameters)
        {
            return BindCustomer(parameters);
        }
        public Customer CreateRandomCustomer(Cooking.DishInfo[] dishes)
        {

            return Create(spriteKeys[Random.RandomRange(0, spriteKeys.Length)], customerWaitTime, dishes);
        }
    }
    [System.Serializable]
    public class Customer
    {
        public string SpriteKey { get; set; }
        public float WaitTime { get; set; }
        public int DishAmount { get; set; }
        public Cooking.DishInfo[] Dishes { get; set; }
        public Customer(string spriteKey, float waitTime, Cooking.DishInfo[] dishes)
        {
            SpriteKey = spriteKey;
            WaitTime = waitTime;
            Dishes = dishes;
            DishAmount = dishes.Length;
        }
    }
}