﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Roasting
{
    public class MeatDispenser : MonoBehaviour
    {
        [SerializeField]
        Pan[] pans;

        public void FillPans()
        {
            for (int i = 0; i < pans.Length; i++)
            {
                if (pans[i].IsEmpty)
                {
                    pans[i].AddMeat();
                    break;
                }
            }
        }
    }
}