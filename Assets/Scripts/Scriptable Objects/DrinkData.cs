﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Drinks
{
    [CreateAssetMenu(fileName = "New DrinkData", menuName = "Scriptable Objects/Drink Data", order = 2)]
    public class DrinkData : ScriptableObject
    {
        [SerializeField]
        private string drinkName;
        [SerializeField]
        private float cookingTime;
        [SerializeField]
        KeyState[] keyStates;

        public string DrinkName
        {
            get
            {
                return drinkName;
            }
        }
        public float CookingTime
        {
            get
            {
                return cookingTime;
            }
        }

        public string GetKeyByState(DrinkState state)
        {
            return System.Array.Find<KeyState>(keyStates, x => x.state == state).key;
        }

    }
    [System.Serializable]
    public struct KeyState
    {
        public string key;
        public DrinkState state;

    }
    public enum DrinkState { Empty, Full }
}