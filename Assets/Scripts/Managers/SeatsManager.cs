﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SeatsManager : MonoBehaviour
{
    public int SeatsAmount { get; private set; }
    public int FilledSeatsAmount { get; private set; }
    public int ServedOrdersAmount { get; private set; }
    [SerializeField]
    int maxCustomerWaitTime;
    [SerializeField]
    int dishExtraTime;
    [SerializeField]
    Seat[] seats;
    private static SeatsManager instance;

    public static SeatsManager Instance { get => instance; }
    void Awake()
    {
        if (instance != null && instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            instance = this;
        }
        SeatsAmount = seats.Length;
    }
    public bool ServeOrder(Cooking.DishInfo incominDish)
    {
        int bestFoundSeat = -1;
        int bestFoundDish = -1;
        for (int i = 0; i < seats.Length; i++)
        {
            if (seats[i].customer != null)
            {
                for (int j = 0; j < seats[i].customer.Dishes.Length; j++)
                {
                    if (Cooking.DishInfo.DishEquals(seats[i].customer.Dishes[j], incominDish)
                        && (bestFoundSeat == -1 || seats[i].customer.WaitTime < seats[bestFoundSeat].customer.WaitTime))
                    {
                        bestFoundSeat = i;
                        bestFoundDish = j;
                    }
                }
            }
        }
        if (bestFoundSeat != -1 && bestFoundDish != -1)
        {
            float temp = seats[bestFoundSeat].customer.WaitTime + dishExtraTime;
            seats[bestFoundSeat].customer.WaitTime = temp > maxCustomerWaitTime ? maxCustomerWaitTime : temp;

            seats[bestFoundSeat].dishViews[bestFoundDish].dish = null;
            seats[bestFoundSeat].dishViews[bestFoundDish].dishView.SetVisiable(false);
            seats[bestFoundSeat].dishViews[bestFoundDish].gameObject.SetActive(false);

            ServedOrdersAmount++;
            seats[bestFoundSeat].customer.Dishes[bestFoundDish] = null;
            seats[bestFoundSeat].customer.DishAmount--;
            if (seats[bestFoundSeat].customer.DishAmount <= 0)
            {
                EmptySeat(bestFoundSeat);
            }
            return true;
        }

        return false;
    }
    public void FillSeat(Factory.Customer customer)
    {
        for (int i = 0; i < seats.Length; i++)
        {
            if (seats[i].customer == null)
            {
                seats[i].customer = customer;
                seats[i].view.SetSpriteByKey(customer.SpriteKey);
                seats[i].view.SetVisiable(true);
                seats[i].timer.maxValue = maxCustomerWaitTime;
                for (int j = 0; j < customer.Dishes.Length; j++)
                {
                    seats[i].dishViews[j].dish = customer.Dishes[j];
                    seats[i].dishViews[j].dishView.SetSpriteByKey(customer.Dishes[j].SpriteKey);
                    seats[i].dishViews[j].dishView.SetVisiable(true);
                    seats[i].dishViews[j].gameObject.SetActive(true);
                }
                seats[i].orderPanel.SetActive(true);
                FilledSeatsAmount++;
                break;
            }
        }
    }
    void Update()
    {
        for (int i = 0; i < seats.Length; i++)
        {
            if (seats[i].customer != null)
            {
                seats[i].customer.WaitTime -= Time.deltaTime;
                seats[i].timer.value = seats[i].customer.WaitTime;
                if (seats[i].customer.WaitTime <= 0)
                {
                    EmptySeat(i);
                }
            }
        }
    }
    void EmptySeat(int seatNum)
    {
        for (int i = 0; i < seats[seatNum].dishViews.Length; i++)
        {
            seats[seatNum].dishViews[i].dish = null;
            seats[seatNum].dishViews[i].dishView.SetVisiable(false);
            seats[seatNum].dishViews[i].gameObject.SetActive(false);
        }
        seats[seatNum].view.SetVisiable(false);
        seats[seatNum].orderPanel.SetActive(false);
        seats[seatNum].customer = null;
        FilledSeatsAmount--;
    }
}
[System.Serializable]
public class Seat
{
    public ViewManager view;
    public Factory.Customer customer { get; set; } = null;
    public GameObject orderPanel;
    public Slider timer;
    public DishView[] dishViews;
}
