﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Cooking
{
    public class BreadMeatDish : Dish
    {
        [SerializeField]
        ViewManager[] breadSprites;
        [SerializeField]
        ViewManager meatSprite;

        protected override void ShowIngredients()
        {
            switch (dishInfo.Level)
            {
                case 1:
                    breadSprites[0].SetVisiable(true);
                    break;
                case 2:
                    breadSprites[1].SetVisiable(true);
                    meatSprite.SetVisiable(true);
                    break;
            }
        }
        public override void ServeDish()
        {
            if (SeatsManager.Instance.ServeOrder(dishInfo))
            {
                base.ServeDish();
                foreach (var item in breadSprites)
                {
                    item.SetVisiable(false);
                }
                meatSprite.SetVisiable(false);
            }
        }
    }
}