﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Factory
{
    public abstract class Factory<T> : MonoBehaviour
    {
        protected abstract T Create(params System.Object[] parameters);
    }
}
