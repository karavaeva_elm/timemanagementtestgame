﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EndPanelHandler : MonoBehaviour
{
    [SerializeField]
    GameObject endPanel;
    [Header("Chef views")]
    [SerializeField]
    ViewManager chef;
    [SerializeField]
    string winChefKey;
    [SerializeField]
    string loseChefKey;
    [Header("Sign views")]
    [SerializeField]
    ViewManager dishSign;
    [SerializeField]
    string winSignKey;
    [SerializeField]
    string loseSignKey;
    [Header("Panel settings")]
    [SerializeField]
    Slider slider;
    [SerializeField]
    Text headerText;
    [SerializeField]
    Text resText;
    [SerializeField]
    GameObject[] winButtons;
    [SerializeField]
    GameObject[] loseButtons;
    public void EndGame(int served, int required)
    {
        if (served < required)
        {
            chef.SetSpriteByKey(loseChefKey);
            dishSign.SetSpriteByKey(loseSignKey);
            headerText.text = "Вы проиграли";
            foreach (var item in loseButtons)
            {
                item.SetActive(true);
            }
        }
        else
        {
            chef.SetSpriteByKey(winChefKey);
            dishSign.SetSpriteByKey(winSignKey);
            headerText.text = "Вы выйграли!";
            foreach (var item in winButtons)
            {
                item.SetActive(true);
            }
        }
        resText.text = served + " / " + required;
        slider.maxValue = required;
        slider.value = served;
        endPanel.SetActive(true);
    }
}