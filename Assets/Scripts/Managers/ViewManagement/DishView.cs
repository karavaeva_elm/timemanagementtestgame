﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DishView : MonoBehaviour
{
    public Cooking.DishInfo dish { get; set; }
    public ViewManager dishView;
}
