﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Drinks
{
    public class Glass : CookingContainer
    {
        DrinkState drinkState = DrinkState.Empty;
        [Header("Drink Info")]
        [SerializeField]
        DrinkData drinkData;
        [SerializeField]
        ViewManager drinkView;
        [SerializeField]
        public Cooking.DishInfo DishInfo;

        void Awake()
        {
            maxTime = drinkData.CookingTime;
            timer = maxTime;
            timerView.ShowTimer(true);
        }
        public void EmptyGlass()
        {
            if (drinkState == DrinkState.Full)
            {
                IsEmpty = true;
                ChangeDrinkState(DrinkState.Empty);
                timerView.ShowTimer(true);
                timer = maxTime;
                DishInfo.Level = 0;
            }
        }
        private void FillGlass()
        {
            IsEmpty = false;
            ChangeDrinkState(DrinkState.Full);
            DishInfo.Level = 1;
        }

        private void ChangeDrinkState(DrinkState newState)
        {
            drinkState = newState;
            drinkView.SetSpriteByKey(drinkData.GetKeyByState(drinkState));
        }
        void Update()
        {
            if (IsEmpty)
            {
                timer -= Time.deltaTime;
                timerView.ChangeValue(Mathf.InverseLerp(maxTime, 0, timer));
                if (timer <= 0)
                {
                    FillGlass();
                    timerView.ShowTimer(false);
                }
            }
        }
    }
}