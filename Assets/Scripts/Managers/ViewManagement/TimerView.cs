﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TimerView : MonoBehaviour
{
    [SerializeField]
    Image progressBar;
    [SerializeField]
    public KeysManager timerKeysManager;
    [SerializeField]
    KeysManager barKeysManager;

    public void ShowTimer(bool isVisible)
    {
        timerKeysManager.myViewManager.SetVisiable(isVisible);
        barKeysManager.myViewManager.SetVisiable(isVisible);
    }
    public void ChangeView(TimerState state)
    {
        switch (state)
        {
            case TimerState.Green:
                timerKeysManager.myViewManager.SetSpriteByKey(timerKeysManager.greenKey);
                barKeysManager.myViewManager.SetSpriteByKey(barKeysManager.greenKey);
                break;
            case TimerState.Red:
                timerKeysManager.myViewManager.SetSpriteByKey(timerKeysManager.redKey);
                barKeysManager.myViewManager.SetSpriteByKey(barKeysManager.redKey);
                break;
        }
    }

    public void ChangeValue(float value)
    {
        progressBar.fillAmount = value;
    }
}
public enum TimerState { Green, Red }

[System.Serializable]
public struct KeysManager
{
    public string greenKey;

    public string redKey;
    public ViewManager myViewManager;
}