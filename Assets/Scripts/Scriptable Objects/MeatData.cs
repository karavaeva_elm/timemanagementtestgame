﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace Roasting
{
    [CreateAssetMenu(fileName = "New MeatData", menuName = "Scriptable Objects/Meat Data", order = 1)]
    public class MeatData : ScriptableObject
    {
        [SerializeField]
        private string meatName;
        [SerializeField]
        private float cookingTime;
        [SerializeField]
        private float burningTime;
        [SerializeField]
        private KeyState[] keyStates;
        public string MeatName
        {
            get
            {
                return meatName;
            }
        }
        public float CookingTime
        {
            get
            {
                return cookingTime;
            }
        }
        public float BurningTime
        {
            get
            {
                return burningTime;
            }
        }
        public string GetKeyByState(MeatState state)
        {
            return System.Array.Find<KeyState>(keyStates, x => x.state == state).key;
        }

    }
    [System.Serializable]
    public struct KeyState
    {
        public string key;
        public MeatState state;

    }
    public enum MeatState { Raw, Roasted, Burnt }
}