﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class CookingContainer : MonoBehaviour
{
    public bool IsEmpty { get; set; } = true;
    [Header("Timer Info")]
    [SerializeField]
    protected TimerView timerView;
    protected float timer;
    protected float maxTime;
}
