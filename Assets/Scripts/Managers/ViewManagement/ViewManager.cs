﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ViewManager : MonoBehaviour
{
    public SpriteType mySpriteType;
    public SpriteRenderer myRenderer;
    public Image myImg;

    public void SetSpriteByKey(string key)
    {
        var sprite = SpritePool.Instance.GetSpriteByKey(key);
        switch (mySpriteType)
        {
            case SpriteType.Renderer:
                myRenderer.sprite = sprite;
                break;
            case SpriteType.Image:
                myImg.sprite = sprite;
                break;
        }
    }

    public void SetVisiable(bool isVisiable)
    {
        var a = isVisiable ? 1f : 0f;
        switch (mySpriteType)
        {
            case SpriteType.Renderer:
                var c = myRenderer.color;
                myRenderer.color = new Color(c.r, c.g, c.b, a);
                break;
            case SpriteType.Image:
                c = myImg.color;
                myImg.color = new Color(c.r, c.g, c.b, a);
                break;
        }
    }
}
public enum SpriteType { Renderer, Image }