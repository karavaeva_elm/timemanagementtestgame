﻿using System.Collections;
using System.Collections.Generic;
using Factory;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    [SerializeField]
    EndPanelHandler endPanel;
    [SerializeField]
    DishInfoFactory dishFactory;
    [SerializeField]
    CustomerFactory customerFactory;
    [SerializeField]
    public int customersQuantity;
    [SerializeField]
    int maxDishesPerCustomer;
    [SerializeField]
    int customersFrequency;

    Customer[] customers;

    int currCustomer;
    float timer;
    int winningAmount;
    private static GameManager instance;

    public static GameManager Instance { get => instance; }
    void Awake()
    {
        if (instance != null && instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            instance = this;
        }
    }
    void Start()
    {
        customers = new Customer[customersQuantity];
        timer = customersFrequency;
        for (int i = 0; i < customers.Length; i++)
        {
            Customer temp = FillCustomer();
            if (temp != null)
                customers[i] = temp;
            else
                customers = new Customer[0];
        }
        winningAmount -= 2;
    }
    void Update()
    {
        if (currCustomer < customers.Length && SeatsManager.Instance.FilledSeatsAmount < SeatsManager.Instance.SeatsAmount)
        {
            timer -= Time.deltaTime;
            if (timer <= 0)
            {
                SeatsManager.Instance.FillSeat(customers[currCustomer]);
                currCustomer++;
                timer = customersFrequency;
            }
        }
        else
        {
            if (SeatsManager.Instance.FilledSeatsAmount <= 0)
            {
                endPanel.EndGame(SeatsManager.Instance.ServedOrdersAmount, winningAmount);
            }
        }
    }

    public void Restart()
    {
        SceneManager.LoadScene("Main");
    }
    public void Exit()
    {
        Application.Quit();
    }
    Customer FillCustomer()
    {
        int dishesAmount = Random.RandomRange(1, maxDishesPerCustomer);
        Cooking.DishInfo[] dishes = new Cooking.DishInfo[dishesAmount];
        for (int i = 0; i < dishesAmount; i++)
        {
            Cooking.DishInfo temp = dishFactory.CreateRandomDish();
            if (temp != null)
            {
                dishes[i] = temp;
                winningAmount++;
            }
            else return null;
        }
        return customerFactory.CreateRandomCustomer(dishes);
    }
}