﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Cooking
{
    public class Dish : MonoBehaviour
    {
        [SerializeField]
        protected DishInfo dishInfo;
        public virtual bool AddIngredient(int requiredLevel)
        {
            if (requiredLevel - dishInfo.Level == 1)
            {
                dishInfo.Level++;
                ShowIngredients();
                return true;
            }
            else return false;
        }
        protected virtual void ShowIngredients() { }
        public virtual void ServeDish()
        {
            dishInfo.Level = 0;
        }
    }
    public enum DishName { Burger, Hotdog, Cola }
    [System.Serializable]
    public class DishInfo
    {
        public DishName Name;
        public int Level { get; set; }
        public string SpriteKey { get; set; }
        public DishInfo(DishName name, int level, string key)
        {
            Name = name;
            Level = level;
            SpriteKey = key;
        }
        public static bool DishEquals(DishInfo a, DishInfo b)
        {
            if (a == null || b == null)
            {
                return false;
            }
            else
                return a.Name.Equals(b.Name) && a.Level == b.Level;
        }
    }
}