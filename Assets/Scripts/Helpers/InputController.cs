﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

namespace InputControlls
{
    public class InputController : MonoBehaviour, IPointerClickHandler
    {
        [System.Serializable] public class TapEvent : UnityEvent { }
        public TapEvent OnSingleClick;
        public TapEvent OnDoubleClick;
        public void OnPointerClick(PointerEventData eventData)
        {
            int clickCount = eventData.clickCount;
            switch (clickCount)
            {
                case 1:
                    OnSingleClick.Invoke();
                    break;
                case 2:
                    OnDoubleClick.Invoke();
                    break;
            }
        }
    }
}