﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Roasting
{
    public class Pan : CookingContainer
    {
        MeatState meatState = MeatState.Raw;
        [Header("Meat Info")]
        [SerializeField]
        MeatData meatData;
        [SerializeField]
        ViewManager meatView;
        [SerializeField]
        [Header("Dish Info")]
        Cooking.DishBuilder dishBuilder;

        public void AddMeat()
        {
            IsEmpty = false;
            ChangeMeatState(MeatState.Raw);
            meatView.SetVisiable(true);
            maxTime = meatData.CookingTime;
            timer = maxTime;
            timerView.ChangeView(TimerState.Green);
            timerView.ShowTimer(true);
        }
        public void RemoveMeat()
        {
            if (meatState == MeatState.Roasted && dishBuilder.FillDishes(2))
            {
                CleanPan();
            }
        }
        public void TrashMeat()
        {
            if (meatState == MeatState.Burnt)
            {
                CleanPan();
            }
        }
        private void CleanPan()
        {
            IsEmpty = true;
            meatView.SetVisiable(false);
            timerView.ShowTimer(false);
        }
        private void ChangeMeatState(MeatState newState)
        {
            meatState = newState;
            meatView.SetSpriteByKey(meatData.GetKeyByState(meatState));
        }
        void Update()
        {
            if (!IsEmpty && meatState != MeatState.Burnt)
            {
                timer -= Time.deltaTime;
                timerView.ChangeValue(Mathf.InverseLerp(maxTime, 0, timer));
                if (timer <= 0)
                {
                    switch (meatState)
                    {
                        case MeatState.Raw:
                            ChangeMeatState(MeatState.Roasted);
                            maxTime = meatData.BurningTime;
                            timer = maxTime;
                            timerView.ChangeView(TimerState.Red);
                            break;
                        case MeatState.Roasted:
                            ChangeMeatState(MeatState.Burnt);
                            timerView.ShowTimer(false);
                            break;
                    }
                }
            }
        }
    }
}