﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpritePool : MonoBehaviour
{
    public static SpritePool Instance { get; private set; }
    [SerializeField]
    private SpriteRef[] refs;

    public void Awake()
    {
        Instance = this;
    }

    public Sprite GetSpriteByKey(string key)
    {
        return System.Array.Find<SpriteRef>(refs, x => x.key == key).sprite;
    }

    [System.Serializable]
    public class SpriteRef
    {
        public string key;
        public Sprite sprite;
    }
}
