﻿using System.Collections;
using System.Collections.Generic;
using Cooking;
using UnityEngine;

namespace Factory
{
    public class DishInfoFactory : Factory<DishInfo>
    {
        [SerializeField]
        DishSettings[] dishesSettings;
        private DishInfo BindDishInfo(System.Object[] parameters)
        {
            return new DishInfo((DishName)parameters[0], (int)parameters[1], (string)parameters[2]);
        }

        protected override DishInfo Create(params System.Object[] parameters)
        {
            return BindDishInfo(parameters);
        }
        public DishInfo CreateRandomDish()
        {
            DishName name = RandomEnum<DishName>.Get();
            for (int i = 0; i < dishesSettings.Length; i++)
            {
                if (dishesSettings[i].name == name)
                {
                    int level = Random.Range(dishesSettings[i].minReadyLevel, dishesSettings[i].maxReadyLevel);
                    return Create(name, level, dishesSettings[i].spriteKey);
                }
            }
            return null;
        }
    }
    [System.Serializable]
    public struct DishSettings
    {
        public DishName name;
        public int minReadyLevel;
        public int maxReadyLevel;
        public string spriteKey;
    }
}