﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Drinks
{
    public class DrinksDispenser : MonoBehaviour
    {
        [SerializeField]
        Glass[] glasses;
        public void ServeDrink()
        {
            for (int i = 0; i < glasses.Length; i++)
            {
                if (!glasses[i].IsEmpty && SeatsManager.Instance.ServeOrder(glasses[i].DishInfo))
                {
                    glasses[i].EmptyGlass();
                    break;
                }
            }
        }
    }
}