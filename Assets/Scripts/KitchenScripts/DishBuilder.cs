﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Cooking
{
    public class DishBuilder : MonoBehaviour
    {
        [SerializeField]
        Dish[] dishes;
        public bool FillDishes(int level)
        {
            for (int i = 0; i < dishes.Length; i++)
            {
                if (dishes[i].AddIngredient(level))
                {
                    return true;
                }
            }
            return false;
        }
        public void FillbyFirst()
        {
            FillDishes(1);
        }
    }
}